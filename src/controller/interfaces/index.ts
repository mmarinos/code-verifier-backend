import { BasicResponse, GoodbyeResponse } from "../types";

export interface IHelloController{
    getMessage(name?: string): Promise<BasicResponse>
}

export interface IGoodbyeController{
    getMessage(name?: string): Promise<GoodbyeResponse>
}

export interface IUsersController{
    //Read all users from database
    getUsers(id?:string): Promise<any>
    //create new user
    createUser(user: any): Promise<any>
    //update user by id
    updateUser(id: string, user: any): Promise<any>
    //delete user by id
    deleteUser(id?: string): Promise<any>
}
