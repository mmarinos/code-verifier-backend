import { GoodbyeResponse } from "./types";
import { IGoodbyeController } from "./interfaces";
import { logSucces } from "../utils/logger";


export class GoodbyeController implements IGoodbyeController{
    public async getMessage(name?: string): Promise<GoodbyeResponse> {
        logSucces('[api/goodbye] Get Request');
        const fecha = new Date();
        return {
            message: `Goodbye, ${name || "World!"}`,
            "Date": `${fecha}`
        }
    }
    
}