import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IUsersController } from "../controller/interfaces";
import { logSucces, logWarning } from "../utils/logger";


// ORM
import { getAllUsers, getUserById, deleteUserById, createUser, updateUserById } from "../domain/orm/User.orm";

@Route("/api/users")
@Tags("UsersController")
export class UsersController implements IUsersController {
    
    /**
     * Endpoint to retreive the Users in the colletion "Users" of BD
     * @param {string} id Id or user to retrieve (optional)
     * @returns All users or a specific user
     */
    @Get("/")
    public async getUsers( @Query()id?: string  ): Promise<any> {

        let response: any = '';

        if(id){
            logSucces(`[/api/users] Get Users by Id ${id}`);
            response = await getUserById(id);
        }else {
            logSucces('[GET] /api/users] Get All Users Request');
            response = await getAllUsers();
        }

        return response 
    }
    /**
     * Endpoint to delete the Users in the colletion "Users" of BD
     * @param {string} id Id or user to delete (optional)
     * @returns message informing if delete was successful
     */
    @Delete("/")
    public async deleteUser( @Query()id?: string  ): Promise<any> {

        let response: any = '';

        if(id){
            logSucces(`[/api/users] Delete User by Id ${id}`);
            response = await deleteUserById(id);
        }else {
            logWarning('[Delete] /api/users] Delete User Request Without Id');
            response = {message: 'No Id provided'};
        }

        return response 
    }

    @Post("/")
    public async createUser(user: any): Promise<any> {
        let response: any = '';

        await createUser(user).then((_r) => {
            logSucces(`[/api/users] Create User: ${user}`);
            response = {message: `User created successfully ${user.name}`};
        })

        return response;
    }

    @Put("/")
    public async updateUser(@Query()id: string, user: any): Promise<any> {
        let response: any = '';

        if(id){
            logSucces(`[/api/users] update User by Id ${id}`);
            await updateUserById(id, user).then((_r) => {
                response = {
                    message: `User updated successfully ${user.name}`
                };
            });
        }else {
            logWarning('[Delete] /api/users] Update User Request Without Id');
            response = {message: 'No Id provided'};
        }

        return response; 
    }
        
    }
    