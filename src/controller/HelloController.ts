import { Get, Query, Route, Tags } from 'tsoa';
import { BasicResponse } from "./types";
import { IHelloController } from "./interfaces";
import { logSucces } from "../utils/logger";

@Route('/api/hello')
@Tags('HelloController')
export class HelloController implements IHelloController{
    /**
     * Endpoint to retrieve a Message "Hello {name}" in JSON format
     * @param { String | undefined } name Name to be used to be greeted
     * @returns { BasicResponse } Promise of BasicResponse
     */
    @Get('/')
    public async getMessage(@Query()name?: string): Promise<BasicResponse> {
        logSucces('[api/hello] Get Request');

        return {
            message: `Hello, ${name || "World!"}`
        }
    }
    
}