import express, { Request, response, Response } from "express";
import { GoodbyeController } from "../controller/GoodByeController"
import { logError, logInfo } from "../utils/logger";


//Router from express
let goodbyeRouter = express.Router();

//GET -> http://localhost:8000/api/hello?name=Martin/
goodbyeRouter.route('/')
    .get(async (req: Request, res: Response) => {
        //Obtain a Query Param
        let name: any = req?.query?.name;
        logInfo(`Query Param: ${name}`);
        //Instance Controller to execute method
        const controller: GoodbyeController = new GoodbyeController();
        //Obtein Response
        try {
            const Response = await controller.getMessage(name);
            return res.send(Response)
        } catch (error) {
            logError(`Query Param: ${name}`);
        }
})

//Export hello Router
export default goodbyeRouter;