/**
 * Root Router
 * Redirections to Routers
 */

import express, { Request, Response } from "express";
import helloRouter from "./HelloRoutes";
import goodbyeRouter from "./GoodByeRoute";
import { logInfo } from "../utils/logger";
import usersRouter from "../routes/UserRouter";

// Server instance
let server = express();

//Router instance
let rootRouter = express.Router();

//Activate for request to http://localhost:8000/api

//GET: http://localhost:8000/api
rootRouter.get('/', (req: Request, res: Response) => {
    logInfo(`GET: http://localhost:8000/api`);
    //send message
    res.status(200).send('Welcome to my API Restful: Express + TS + Nodemon + Jest + Swagger + Mongoose')
});

//Redirections to Routers & Controllers
server.use('/', rootRouter); //http://localhost:8000/api
server.use('/hello', helloRouter); //http://localhost:8000/api/hello
server.use('/goodbye', goodbyeRouter); //http://localhost:8000/api/hello
// Add more routes
server.use('/users', usersRouter); //http://localhost:8000/api/users

export default server;