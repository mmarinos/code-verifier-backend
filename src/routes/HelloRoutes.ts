import express, { Request, response, Response } from "express";
import { HelloController } from "../controller/HelloController"
import { logError, logInfo } from "../utils/logger";


//Router from express
let helloRouter = express.Router();

//GET -> http://localhost:8000/api/hello?name=Martin/
helloRouter.route('/')
    .get(async (req: Request, res: Response) => {
        //Obtain a Query Param
        let name: any = req?.query?.name;
        logInfo(`Query Param: ${name}`);
        //Instance Controller to execute method
        const controller: HelloController = new HelloController();
        //Obtein Response
        try {
            const Response = await controller.getMessage(name);
            return res.send(Response)
        } catch (error) {
            logError(`Query Param: ${name}`);
        }
})

//Export hello Router
export default helloRouter;