import express, { Request, Response } from "express";
import { UsersController } from "../controller/UsersController"
import { logError, logInfo } from "../utils/logger";


//Router from express
let usersRouter = express.Router();

//GET -> http://localhost:8000/api/users?id=34534523435/
usersRouter.route('/')
    .get(async (req: Request, res: Response) => {
       
        //Obtein Response
        try {
            //Obtain a Query Param
            let id: any = req?.query?.id;
            logInfo(`Query Param: Users ${id}`);
            //Instance Controller to execute method
            const controller: UsersController = new UsersController();
            const response: any = await controller.getUsers(id);
            return res.send(response);
        } catch (error) {
            logError(`Error: ${error}`);
        }
    })
    //Delete
    .delete(async (req: Request, res: Response) => {
        try {
            let id: any = req?.query?.id;
            logInfo(`Query Param: ${id}`);
            const controller: UsersController = new UsersController();
            const response: any = await controller.deleteUser(id);
            return res.send(response);
        } catch (error) {
            logError(`Error: ${error}`);
        }
    })
    //Post
    .post(async (req: Request, res: Response) => {
        try {
            
            let user = {
                name: req.body.name,
                email: req.body.email,
                age: req.body.age
            }

            const controller: UsersController = new UsersController();
            const response: any = await controller.createUser(user);
            return res.send(response);
        } catch (error) {
            
        }
    })

    .put(async (req: Request, res: Response) => {
        try {
            let id: any = req?.query?.id;
            logInfo(`Query Param: ${id}`);
            const controller: UsersController = new UsersController();

            let user = {
                name: req.body.name,
                email: req.body.email,
                age: req.body.age
            }
            const response: any = await controller.updateUser(id, user);
            return res.send(response);
        } catch (error) {
            logError(`Error: ${error}`);
        }
    })
//Export user Router
export default usersRouter;