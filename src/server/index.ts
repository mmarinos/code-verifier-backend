import express, { Express, Request, Response } from "express";

//Swagger
import swaggerUi from "swagger-ui-express";


import dotenv from'dotenv'; //Enviroment variables
//security
import cors from 'cors'
import helmet from 'helmet'

//TODO Https
//root Routes
import rootRuter from '../routes'
import mongoose from "mongoose";

dotenv.config();

// * Create app express
const server: Express = express();

// * Create Express APP
server.use(
    '/docs',
    swaggerUi.serve,
    swaggerUi.setup(undefined, {
        swaggerOptions: {
            url: '/swagger.json',
            explorer: true
        }
    })
);

// * Define SERVER to use "/api" and use rootRouter from 'index.ts' inroutes
// * from this point onover: http://localhost:8000/api/...
server.use(
    '/api',
    rootRuter
);

// Static Server
server.use(express.static('public'));


//TODO Mongoose Connetion
mongoose.connect('mongodb://root:root@localhost:27017/codeverifications?authSource=admin')


// * Secutiry Config
server.use(helmet());
server.use(cors());

// * Content type
server.use(express.urlencoded({ extended: true, limit: '50mb' }));
server.use(express.json({ limit: '50mb' }));

//http://localhost:8000/ --> http://localhost:8000/api/
server.get('/', (req: Request, resp: Response) => {
    resp.redirect('/api');
});

export default server;