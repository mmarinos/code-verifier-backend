
import { userEntity } from "../entities/User.entity";
import { logSucces, logError } from "../../utils/logger";



// CRUD

/**
 * Method to create a new user in the database
 */
export const getAllUsers = async ():Promise<any[] | undefined> => {

    try {
        let userModel = userEntity();
        // search for all users
        logSucces('[GET] /api/users] Get All Users Request');
        return new Promise((_resolve) => {
            userModel.find({isDeleted: false});
        });
    } catch (error) {
        logError(`[ORM ERROR]: Getting All Users: ${error}`);
    }
}

//TODO
// - Get User by Id
export const getUserById = async (id: string):Promise<any | undefined> => {
    try {
        let userModel = userEntity();
        // search for ID
        logSucces(`[ORM ERROR]: Getting User by Id: ${id}`);
        return new Promise((_resolve) => {
            userModel.findById(id);
        });
    } catch (error) {
        logError(`[ORM ERROR]: Getting User by Id: ${error}`);
    }
}

// - Delete User by ID
export const deleteUserById = async (id: string):Promise<any | undefined> => {
    try {
        let userModel = userEntity();
        //Delete for ID
        logSucces(`[ORM SUCCESS]: Deleting User by Id: ${id}`);
        
        return new Promise((_resolve) => {
            userModel.deleteOne({ _id: id });
        });
    } catch (error) {
        logError(`[ORM ERROR]: Deleting User by Id: ${error}`);
    }
}

// - Create New User
export const createUser = async (user: any):Promise<any | undefined> => {
    try {
        let userModel = userEntity();
        // create user
        logSucces(`[ORM SUCCESS]: Deleting User by Id: ${user}`);
        return await userModel.create(user);
    } catch (error) {
        logError(`[ORM ERROR]: Creating User: ${error}`);
    }
}

// - Get User by Email
// - Update User by ID
export const updateUserById = async (id: string, user: any):Promise<any | undefined> => {
    try {
        let userModel = userEntity();
        // update user
        logSucces(`[ORM SUCCESS]: Deleting User by Id: ${user}`);
        return new Promise((_resolve) => {
            userModel.findByIdAndUpdate(id, user);
        });
    } catch (error) {
        logError(`[ORM ERROR]: Updating User: ${id} : ${error}`);
    }
}

