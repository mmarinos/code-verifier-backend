Muestra las primeras 5 ciudades que empiecen por A ordenadas de manera ascendente, las soluciones deben ser únicas.
db.Contacts.find().sort({"location.country":1}).limit(5)
Crea una colección a parte, que solo contenga a los contactos de Francia (France) y que tengan entre 18 y 50 años. Usa una agregación para ello.
 db.Contacts.aggregate([
{
$match: { "location.country": "France", "dob.age": { $gte : 18 },"dob.age": { $lte : 50 }}
},
{ $out: "ContactsFrance18_50"}
])
Añade un número favorito a cada contacto, luego crea un bucket agrupando por número favorito que separe en 5 segmentos.
db.Contacts.updateMany({}, {$set: {numerofavorito:Math.floor(Math.random() * 20) + 12}})
db.Contacts.aggregate([
{ $bucketAuto: {
groupBy: '$numerofavorito',
buckets: 5
}
}
])

En la colección de Contatcs, haz una proyección la cual tiene que devolver solo el name y username del contacto.
db.Contacts.aggregate([
{ $project: {
_id: 0,
Nombre:
{"nombre.first": 1},
Usuario:
{"login.username": 1}
}}
])

Haz una consulta en la colección de Contacts la cual devuelva un documento por cada nombre (name) y que sea único, ordenado por apellido (last), tienes que usar el operador $unwind.
 db.Contacts.aggregate([
{ $group: { _id: "id", nombre: {$addToSet: "$nombre.first"}, apellido:{$addToSet: "$nombre.last"}}},
{ $unwind: "$nombre" },
{ $sort: { apellido: 1 }}
])
Haz una proyección convirtiendo la fecha (date) a un formato DD-MM-AAAA, la nueva variable será fechaNacimiento
db.Contacts.aggregate([
{ $project: {
_id: 0,
fechaNacimiento:
{ $concat: [
{$substr: ["$dob.date",8,2]},
"-",
{$substr: ["$dob.date",5,2]},
"-",
{$substr: ["$dob.date",0,4]}
]
}}}
])