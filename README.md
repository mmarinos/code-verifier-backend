# code-verifier-backend



## Resumen

NPM
```
Dependencias del proyecto:
dotenv: Sirve para manejar las variables de entorno en el proyecto de forma eficiente
express: Sirve para manejar peticiones HTTP y sus diferentes llamados con sus respectivas rutas

Dependencias Desarrollador:
concurrently: Sirve para la ejecucion de scripts en paralelo
eslint: Sirve para mantener el codigo limpio
jest: Sirve para escribir y ejecutar test para nuestro codigo
nodemon: Sirve para ejecutar el codigo en modo debug
serve: Sirve como sitio para servir codigo estatico
ts-jest: Sirve para usar Jest en proyectos escritos en Typescript
ts-node: Sirve para usar Node en proyectos escritos en Typescript
typescript: Sirve para tipar Javascript
webpack-cli: Sirve para empaquetar todos los ficheros
```


## Addicionar Archivos

- [ ] [Crea](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) o [Carga](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) Archivos
- [ ] [Adicionar archivos usando la linea de comandos](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) o carga archivos en un repositorio de GIT con el siguiente comando:

```
cd existing_repo
git remote add origin https://gitlab.com/mmarinos/code-verifier-backend.git
git branch -M main
git push -uf origin main
```
