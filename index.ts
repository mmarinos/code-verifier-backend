import dotenv from 'dotenv';
import server from './src/server';
import { logError, logSucces} from './src/utils/logger'

// * Configuration the .env file
dotenv.config();

const port = process.env.PORT || 8000;

// * Execute aPP and Listen Requests to PORt
server.listen(port, () => {
    logSucces(`EXPRESS SERVER: Running at http://localhost:${port}/api`)
});

// * Contorl server error
server.on('error', (error) => {
    logError(`[SERVER ERROR]: ${error}`)
})